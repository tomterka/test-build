using System;
using Xunit;
using StringConcat;

namespace StringConcatTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestEmptyString()
        {
            Assert.Equal(0, StringAdder.CalculateString(""));
        }

        [Theory]
        [InlineData("5", 5)]
        [InlineData("11", 11)]
        [InlineData("13", 13)]
        public void TestSingleNumber(string str, int num)
        {
            Assert.Equal(num, StringAdder.CalculateString(str));
        }

        [Theory]
        [InlineData("4,5", 9)]
        [InlineData("6,11", 17)]
        public void TestTwoNumbersComma(string str, int num)
        {
            Assert.Equal(num, StringAdder.CalculateString(str));
        }

        [Theory]
        [InlineData("10\n5", 15)]
        [InlineData("11\n23", 34)]
        public void TestTwoNumbersNewline(string str, int num)
        {
            Assert.Equal(num, StringAdder.CalculateString(str));
        }

        [Theory]
        [InlineData("10\n5,4,4", 23)]
        [InlineData("8,7,9\n6", 30)]
        public void TestMultiNumbers(string str, int num)
        {
            Assert.Equal(num, StringAdder.CalculateString(str));
        }

        [Theory]
        [InlineData("-9")]
        [InlineData("8\n7,-9\n-6")]
        public void TestNegativeNumbers(string str)
        {
            Assert.Throws<Exception>(() => StringAdder.CalculateString(str));
        }

        [Theory]
        [InlineData("10\n5345,1000,4\n1001", 1014)]
        [InlineData("8,1007,9", 17)]
        public void TestBigNumbers(string str, int num)
        {
            Assert.Equal(num, StringAdder.CalculateString(str));
        }

        [Theory]
        [InlineData("//#3#1,5\n1", 10)]
        [InlineData("//%8%10%1000", 1018)]
        public void TestOwnDelimiter(string str, int num)
        {
            Assert.Equal(num, StringAdder.CalculateString(str));
        }

    }
}
