﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StringConcat
{
    public static class StringAdder
    {
        public static int CalculateString(string str)
        {
            if (String.IsNullOrEmpty(str))
                return 0;

            Regex regex = new Regex("^\\d+$");
            if(regex.IsMatch(str))
                return int.Parse(str);

            var delimiters = new List<string> { "\n", "," };
            if (str.StartsWith("//"))
            {
                delimiters.Add(str.Substring(2,1));
                str = str.Substring(3);
            }

            if(str.Split(delimiters.ToArray(), StringSplitOptions.None).Select(x => int.Parse(x)).Any(x => x<0))
                throw new Exception();

            return str.Split(delimiters.ToArray(), StringSplitOptions.None).Select(x => int.Parse(x)).Where(x => x <=1000).Sum();
        }
    }
}
